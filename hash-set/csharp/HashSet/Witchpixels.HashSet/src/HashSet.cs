namespace Witchpixels.HashSet;

public class HashSet<T>
{
    private readonly Func<string, int> _hashFn;
    private readonly T[] _hashTable;

    public HashSet(Func<string, int> hashFn, int capacity)
    {
        _hashFn = hashFn;
        _hashTable = new T[capacity];
    }

    public T Get(string key)
    {
        var index = _hashFn(key) % _hashTable.Length;
        return _hashTable[index];
    }

    public void Set(string key, T value)
    {
        var index = _hashFn(key);
        _hashTable[index] = value;
    }
}
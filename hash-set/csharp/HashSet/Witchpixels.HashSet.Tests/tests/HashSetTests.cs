using Xunit;

namespace Witchpixels.HashSet.Tests;

public class HashSetTests
{
    [Fact]
    public void SetsAndGets()
    {
        const string value = "v";
        const string key = "k";

        var hashSet = new HashSet<string>(Hash1Bit, 10);
        
        hashSet.Set(key, value);

        var result = hashSet.Get(key);
        
        Assert.Equal(value, result);
    }

    [Fact]
    public void CanHoldMultipleValues()
    {
        
        var hashSet = new HashSet<string>(Hash1Bit, 10);
        hashSet.Set("k1", "v1");
        hashSet.Set("k2", "v2");
        
        Assert.Equal("v1", hashSet.Get("k1"));
        
        // Oh no! Why does the above pass, and this fail?
        Assert.Equal("v2", hashSet.Get("k2"));
    }

    [Fact]
    public void DoesntExplodeWhenKeyNotFound()
    {
        var hashSet = new HashSet<string>(Hash1Bit, 10);
        hashSet.Get("k1"); // what should this return?
    }

    /// <summary>
    /// A perfectly adequate 1bit hash function.
    /// </summary>
    private static int Hash1Bit(string key)
    {
        return 0;
    }
}
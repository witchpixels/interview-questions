# HashSet

I have a broken hash set, let's fix it!

## Goals

A collaborative coding exercise and a discussion about edge cases and handling expected failures in code.

While there should be a low emphasis on language specifics, a successful candidate should be able to produce, with some coaching, working code that passes the tests.

An intermediate or senior candidate should add additional tests to cover edge cases.

## Non Goals

As ever, the interviewer is expected to cover any knowledge gaps. Whether or not you decide to hire based on what information about hashing, hash functions and hash sets/maps you need to fill the candidate in for is between you and the trees, however.
import {createStore} from '@reduxjs/toolkit'
import TodoStateReducer from "./todo-list/TodoStateReducer";

const store = createStore(TodoStateReducer);

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export default store;
import React, {FormEvent, useState} from 'react';
import {
    Card,
    CardContent,
    Checkbox,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    TextField,
    Typography,
} from "@material-ui/core";
import {useAppDispatch, useAppSelector} from "./hooks";
import {AddTodoItem, CompleteTodoItem} from "./todo-list/TodoActionsApi";

function App() {
    const [pendingTodoText, setPendingTodoText] = useState("");
    const dispatch = useAppDispatch();
    const todoList = useAppSelector(state => state.items);

    const handleTodoSubmit = (ev: FormEvent<HTMLFormElement>) => {
        AddTodoItem(pendingTodoText, dispatch);
        ev.preventDefault();
    };

    return (
        <div>
            <Card>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        Todo List:
                    </Typography>
                    <form onSubmit={ev => handleTodoSubmit(ev)}>
                        <TextField label="Add todo item here" onChange={ev => setPendingTodoText(ev.target.value)}/>
                    </form>
                    <List>
                        {todoList.map((todoItem, index) => {
                            const labelId = `checkbox-list-label-${todoItem}`;

                            return (
                                <ListItem key={index} role={undefined} dense button
                                          onClick={() => CompleteTodoItem(todoItem.text, dispatch)}>
                                    <ListItemIcon>
                                        <Checkbox
                                            edge="start"
                                            checked={todoItem.complete}
                                            tabIndex={-1}
                                            disableRipple
                                            inputProps={{'aria-labelledby': labelId}}
                                            onClick={() => CompleteTodoItem(todoItem.text, dispatch)}
                                        />
                                    </ListItemIcon>
                                    <ListItemText id={labelId} primary={todoItem.text}/>
                                </ListItem>
                            );
                        })}
                    </List>
                </CardContent>
            </Card>
        </div>
    );
}

export default App;

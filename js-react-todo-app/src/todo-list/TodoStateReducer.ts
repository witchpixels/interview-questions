import {ADD_TODO_ITEM, COMPLETE_TODO_ITEM, TodoActions} from "./TodoActionsApi";

export interface TodoItemState {
    text: string;
    complete: boolean;
}

export interface TodoState {
    items: TodoItemState[];
}

export const todoStateDefault = {
    items: [
        {
            text: "Add your first todo item",
            complete: false
        }
    ]
}

export default function TodoStateReducer(state: TodoState = todoStateDefault, action: TodoActions) {
    switch (action.type) {
        case ADD_TODO_ITEM:
            return {
                ...state,
                items: [
                    ...state.items,
                    {
                        text: action.payload.itemText,
                        complete: false,
                    }
                ]
            }
        case COMPLETE_TODO_ITEM:
            const itemIndex = state.items.findIndex((v) => v.text === action.payload.itemText);
            const items = [
                ...state.items
            ];
            items[itemIndex].complete = true;
            return {
                ...state,
                items
            };
    }
    return state;
}
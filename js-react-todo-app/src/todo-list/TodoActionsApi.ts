import {Dispatch} from "react";

export const ADD_TODO_ITEM = "ADD_TODO_ITEM";

export interface AddTodoItemAction {
    type: typeof ADD_TODO_ITEM
    payload: {
        itemText: string;
    }
}


export function AddTodoItem(itemText: string, dispatch: Dispatch<AddTodoItemAction>) {
    dispatch({
        type: ADD_TODO_ITEM,
        payload: {
            itemText
        }
    });
}

export const COMPLETE_TODO_ITEM = "COMPLETE_TODO_ITEM";

export interface CompleteTodoItemAction {
    type: typeof COMPLETE_TODO_ITEM
    payload: {
        itemText: string;
    }
}

export function CompleteTodoItem(itemText: string, dispatch: Dispatch<CompleteTodoItemAction>) {
    dispatch({
        type: COMPLETE_TODO_ITEM,
        payload: {
            itemText
        }
    });
}

export type TodoActions = AddTodoItemAction | CompleteTodoItemAction;
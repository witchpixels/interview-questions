Tamarack's Interview Questions
----------------------------

Collection of interview questions that I seem to recreate every few years, committed usually just in time to use them in an interview.

Setup will vary by project, as do the goals. See each project readme for details.

## A note about interview practice

I tend to have a more conversational style of interview, where we work together to talk about the project as the interview progresses. I do not structure any of these as pure "tests" or any specific knowledge.

Leaving an interviewee in a room alone to do any of these might not be the most productive use of your time.

## If you have somehow sussed out that I'll be interviewing.

Socially engineering before an interview is pretty smart. Go you!
